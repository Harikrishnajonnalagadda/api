﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class User
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public bool Isauthenticated { get; set; }

        public static User Getdetails(string email,string password)
        {
            if (email == "admin@gmail.com" && password == "12345")
                return new User { Name = "admin", Email = "admain@gmail.com", Password = "12345", Isauthenticated = true };
            else
                return new User();
        }
    }
    public class AuthenticatedUsers
    { 
        private static AuthenticatedUsers instance = null;

        private AuthenticatedUsers()
        {
        }

        
        public static AuthenticatedUsers Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AuthenticatedUsers();
                }
                return instance;
            }
        }

        public User Userauthenticated { get; set; }
        public List<FortCode.Models.City> listofcities { get; set; }
    }
}
