﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FortCode.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        [HttpGet]
        public ActionResult Getcitys()
        {
             if (FortCode.Models.AuthenticatedUsers.Instance.Userauthenticated != null && FortCode.Models.AuthenticatedUsers.Instance.Userauthenticated.Isauthenticated == true)
            {
               return Ok(FortCode.Models.AuthenticatedUsers.Instance.listofcities);
            }
            else
            {
                return BadRequest(new
                {
                    message = "User not authenticated"
                });
            }
        }

        [HttpPost("AddCity")]
        public ActionResult AddCity(string CityName, string CountryName)
        {
            if (FortCode.Models.AuthenticatedUsers.Instance.Userauthenticated != null && FortCode.Models.AuthenticatedUsers.Instance.Userauthenticated.Isauthenticated == true)
            {
                if (string.IsNullOrEmpty(CityName) || string.IsNullOrEmpty(CountryName))
                {
                    return BadRequest(new { message = "CityName and CountryName is required to create city" });
                }

                if (FortCode.Models.AuthenticatedUsers.Instance.listofcities == null)
                    FortCode.Models.AuthenticatedUsers.Instance.listofcities = new List<Models.City>();

                if(FortCode.Models.AuthenticatedUsers.Instance.listofcities.Any(c=>c.CityName==CityName && c.CountryName==CountryName))
                    return Ok(new
                    {
                        message = "Details already exist"
                    });
                FortCode.Models.AuthenticatedUsers.Instance.listofcities.Add(new FortCode.Models.City
                {
                    CityName = CityName,
                    CountryName = CountryName
                });
                return Ok();
            }
            else
            {
                return BadRequest(new
                {
                    message = "User not authenticated"
                });
            }
        }
        [HttpDelete("deleteCity")]
        public ActionResult deleteCity(string CityName)
        {
            if (FortCode.Models.AuthenticatedUsers.Instance.Userauthenticated != null && FortCode.Models.AuthenticatedUsers.Instance.Userauthenticated.Isauthenticated == true)
            {
                if(FortCode.Models.AuthenticatedUsers.Instance.listofcities==null || FortCode.Models.AuthenticatedUsers.Instance.listofcities.Count==0)
                    return BadRequest(new
                    {
                        message = "No Records found to delete"
                    });
                FortCode.Models.AuthenticatedUsers.Instance.listofcities.RemoveAll(x => x.CityName.Contains(CityName));
                return Ok();
            }
            else
            {
                return BadRequest(new
                {
                    message = "User not authenticated"
                });
            }
        }
    }
}