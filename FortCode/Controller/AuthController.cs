﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FortCode.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        [HttpGet]

        public ActionResult Get()
        {
            return Ok();
        }
        [HttpPost("authenticate")]
        public ActionResult Authenticate(string Email,string Password)
        {
            if (string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(Password))
            {
                return BadRequest(new { message = "Email and password is necessary for login" });
            }
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
           bool isValidEmail = regex.IsMatch(Email);
            if (!isValidEmail)
            {
                return BadRequest(new  {  message = "The email is invalid"});
            }

            FortCode.Models.User user = FortCode.Models.User.Getdetails(Email,Password);
            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });
            else
            {
                FortCode.Models.AuthenticatedUsers.Instance.Userauthenticated = user;
            }
            return Ok(user);
        }
    }
}